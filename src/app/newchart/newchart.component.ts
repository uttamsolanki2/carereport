import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { CommonServiceService } from '../common-service.service';
declare var Highcharts: any;
import html2canvas from 'html2canvas';
import canvg  from "canvg";

@Component({
  selector: 'app-newchart',
  templateUrl: './newchart.component.html',
  styleUrls: ['./newchart.component.css']
})
export class NewchartComponent implements OnInit {
  private chartType: string;
  private leftLegendText = '';
  private rightLegendText = '';
  private ranges = [];
  showTitle: any;
  chartId: any;
  chart: any;
  imgsrc:any;
  noData: any;
  noDataImg: any;
  chartImgId:any;
  @Input()
  chartData: any;
  chartOptions: any;
  data: any;
  usr: any;
  constructor(private commonService: CommonServiceService) {
    this.chartData = {
      type: '',
      label: '',
      labelString: ''
    };
  }

  ngOnInit() {
    this.noDataImg = null;
    this.chartType = 'weekly';
    this.showTitle = true;
    this.chartId = 'container';
    this.chartImgId = 'chartImgId'
    if (this.chartData['duration']) {
      this.chartType = this.chartData['duration'];
      this.showTitle = false;
    }
    if (this.chartData['chartId']) {
      this.chartId = this.chartData['chartId'];
      this.chartImgId = this.chartId+'-1';
    }
    this.switchChartType();
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

add(){
  let self = this;
  // const div = document.getElementById(this.chartId);
  // html2canvas(div,{allowTaint:true}).then(function(canvas) {
  //   console.log(self.noDataImg);
  //   self.noDataImg = canvas.toDataURL("image/png");
  // });


//  var svg = this.highcharts;
   // var svg = this.highcharts.getSVG({
   //      exporting: {
   //          sourceWidth: Highcharts.chartWidth,
   //          sourceHeight: Highcharts.chartHeight
   //      }
   //  });
   
    var render_width = 300;
    var render_height = render_width * this.chart.chartHeight / this.chart.chartWidth

    // Get the cart's SVG code
    var svg = this.chart.getSVG({
        exporting: {
            sourceWidth: this.chart.chartWidth,
            sourceHeight: this.chart.chartHeight
        }
    });

    // Create a canvas
    var canvas = document.createElement('canvas');
    canvas.height = render_height;
    canvas.width = render_width;
   
    canvg(canvas,svg)
     document.getElementById(self.chartImgId).appendChild(canvas);

  }
  async getChartData(type, start, end) {

    await this.sleep(500);
    let events = [];

    events = this.commonService.storeHealthDataEntry;

    const self = this;

    const filteredData = events.filter(x => {
      return x.type === this.chartData.type && x.communityId;
    });
    if (this.chartData.type === 'hr') {
      const bpData = events.filter(x => {
        return x.type === 'bp' && x.communityId;
      });
      for (let i = 0; i < bpData.length; i++) {
        bpData[i].value1 = bpData[i].value3;
        filteredData.push(bpData);
      }
    }
    this.data = filteredData.map(r => {
      return [parseFloat(moment(r.time).format('x')), parseFloat(r.value1)];
    });
    if (this.data.length > 0) {
      this.noData = 'success';
    }
    if (this.data.length === 0) {
      this.noData = 'fail';
    }

    this.data.sort(function (a, b) {
      // Turn your strings into dates, and then subtract them
      // to get a value that is either negative, positive, or zero.
      return new Date(a[0]).getTime() - new Date(b[0]).getTime();
    });


    if (this.data.length === 0) {
      switch (this.chartData.type) {
        case 'hr':
          this.noDataImg = './assets/HeartIcon/HealthICON.svg';
          break;
        case 'bp':
          this.noDataImg = './assets/HeartIcon/HealthICON.svg';
          break;
        case 'bl':
          this.noDataImg = './assets/Vitals_Icons/BP.svg';
          break;
        case 'ol':
          this.noDataImg = './assets/Vitals_Icons/PulseO2.svg';
          break;
        case 'gl':
          this.noDataImg = './assets/Vitals_Icons/Glucose.svg';
          break;
        case 'btemp':
          this.noDataImg = './assets/Vitals_Icons/Temperature.svg';
          break;
        case 'rrate':
          this.noDataImg = './assets/Vitals_Icons/Respiratory.svg';
          break;
        case 'sp':
          this.noDataImg = './assets/Vitals_Icons/Sleep.svg';
          break;
      }

    }

    this.ranges = this.commonService.storeTargetRange;
    const targetRange = { min: 0, max: 0 };
    const rangeData = this.ranges.filter(x => {
      return x.name === this.chartData.type;
    });
    if (rangeData.length > 0) {
      targetRange.min = rangeData[0].min;
      targetRange.max = rangeData[0].max;
    }


    if (this.data.length > 0) {
      setTimeout(() => {
        {
         this.chart =  Highcharts.stockChart({
            chart: {
              renderTo: this.chartId,
              type: 'line',
              height: 180,
              width: 250,
            },
            title: {
              text: this.showTitle ? this.chartData.label : ''
            },

            tooltip: {
              valueSuffix: this.chartData.labelString
            },
            yAxis: {
              opposite: false,
              lineWidth: 2,
              showEmpty: true,

              title: {
                text: this.chartData.labelString
              },
              plotOptions: {
                series: {
                  marker: {
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    lineColor: null
                  }
                }
              },
              plotBands: [{
                from: targetRange.min,
                to: targetRange.max,
                color: {
                  linearGradient: { x1: 1, y1: 1, x2: 1, y2: 0 },
                  stops: [
                    [0, 'rgb(209, 209, 209)'],
                    [1, 'rgb(254, 254, 254)']
                  ]
                }

              }]
            },
            navigator: {
              enabled: false
            },
            exporting: {
              enabled: true,
              url:'https://export.highcharts.com/',
            }, credits: {
              enabled: false
            },
            scrollbar: {
              enabled: false
            },
            rangeSelector: {
              enabled: false
            },
            xAxis: {
              type: 'datetime',
              min: new Date(start).getTime(),
              max: new Date(end).getTime(),
              ordinal: false,
            },
            series: [{
              name: this.chartData.labelString,
              pointWidth: 40,
              dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M'
              },
              marker: {
                enabled: true,
                fillColor: '#FFFFFF',
                lineWidth: 2,
                lineColor: null
              },
              data: this.data.sort()
            }]
          });
        }
      }, 100);
      await this.sleep(500);
      this.add();
    }

  }

  async switchChartType() {
    let start;
    let end;
    switch (this.chartType) {
      case 'daily':
        start = moment()
          .startOf('day')
          .subtract(1, 'd');
        end = moment().endOf('day');
        this.leftLegendText = start.format('M/D/YY');
        this.rightLegendText = end.format('M/D/YY');
        break;
      case 'weekly':
        start = moment()
          .startOf('day')
          .subtract(1, 'w');
        end = moment().endOf('day');
        this.leftLegendText = start.format('M/D/YY');
        this.rightLegendText = end.format('M/D/YY');
        break;
      case 'monthly':
        start = moment()
          .startOf('date')
          .subtract(1, 'M');
        end = moment().endOf('date');
        this.leftLegendText = start.format('MMMM');
        this.rightLegendText = end.format('MMMM');
        break;
      case 'yearly':
        start = moment()
          .startOf('month')
          .subtract(1, 'y');
        end = moment().endOf('month');
        this.leftLegendText = start.format('YYYY');
        this.rightLegendText = end.format('YYYY');
        break;
      default:
    }
    this.getChartData(this.chartType, start, end);
    const self = this;
  }

}
