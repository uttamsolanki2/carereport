import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GenerateCareReportComponent } from './generate-care-report/generate-care-report.component';

const routes: Routes = [
    //{ path: '', redirectTo: '/dashboard', pathMatch: 'prefix' },
    { path: '', component: DashboardComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'careReport', component: GenerateCareReportComponent }
];
  
@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
  })

export class AppRoutingModule { }
