import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../common-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginActive = true;
  signupActive = false;
  forgotPassword = false;
  constructor(private router: Router, private commonService: CommonServiceService) { }

  ngOnInit() {
    debugger;
  }
  login() {
    this.router.navigate(['dashboard']);
  }
  signupviewActive() {
    this.loginActive = false;
    this.signupActive = true;
  }
  forgotPasswordActive() {
    this.loginActive = false;
    this.signupActive = false;
    this.forgotPassword = true;
  }
  signUp() {
    this.loginActive = true;
    this.signupActive = false;
    this.forgotPassword = false;
  }
  forgot() {
    this.loginActive = true;
    this.signupActive = false;
    this.forgotPassword = false;

  }
}
