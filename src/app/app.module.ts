import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { GenerateCareReportComponent } from './generate-care-report/generate-care-report.component';
import { NewchartComponent } from './newchart/newchart.component';
import { CommonServiceService } from './common-service.service';
import { ObservationchartComponent } from './observationchart/observationchart.component';
import { BpchartComponent } from './bpchart/bpchart.component';
import { AppRoutingModule } from './app.routing.module';
import {FormsModule} from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    GenerateCareReportComponent,
    NewchartComponent,
    ObservationchartComponent,
    BpchartComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule, 
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [CommonServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
