import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservationchartComponent } from './observationchart.component';

describe('ObservationchartComponent', () => {
  let component: ObservationchartComponent;
  let fixture: ComponentFixture<ObservationchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObservationchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObservationchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
