import { Component, OnInit, Input } from '@angular/core';
import { _ } from 'underscore';
import * as moment from 'moment';
import { CommonServiceService } from '../common-service.service';
declare var Highcharts: any;

@Component({
  selector: 'app-observationchart',
  templateUrl: './observationchart.component.html',
  styleUrls: ['./observationchart.component.css']
})
export class ObservationchartComponent implements OnInit {
  leftLegendText: any;
  observationData: any;
  @Input() chartType: any;
  @Input() period: any;
  @Input() chartId: any;
  constructor(private commonService: CommonServiceService) { }

  ngOnInit() {
    this.setupChartData();
  }

  async setupChartData() {
    const observation = this.commonService.storeObservationData;
    this.observationData = _.uniq(observation, 'guid').reverse();
    this.observationData = _.sortBy(this.observationData, 'lastUpdateTimestamp').reverse();
    console.log(this.observationData);
    this.leftLegendText = this.chartType;
    let start, end;
    switch (this.period) {
      case 'daily':
        start = moment()
          .startOf('day')
          .subtract(1, 'd');
        end = moment().endOf('day');
        break;
      case 'weekly':
        start = moment()
          .startOf('day')
          .subtract(1, 'w');
        end = moment().endOf('day');
        break;
      case 'monthly':
        start = moment()
          .startOf('date')
          .subtract(1, 'M');
        end = moment().endOf('date');
        break;
      case 'yearly':
        start = moment()
          .startOf('month')
          .subtract(1, 'y');
        end = moment().endOf('month');
        break;
      default:
        break;
    }
    this.createChart(this.chartType, this.observationData, start, end);
  }


  createChart(chartType, events, start, end) {
    const currentDate = moment(start);
    let eventIndex = 0;
    const data = [];
    while (currentDate.isSameOrBefore(end)) {
      eventIndex = -1;
      while (eventIndex + 1 < events.length) {
        eventIndex++;
        if (this.period === 'yearly') {
          if (moment(currentDate).month() !== moment(events[eventIndex]['date']).month()) { continue; }
        } else {
          if (!this.isSameDayAndMonth(moment(currentDate), moment(events[eventIndex]['date']))) { continue; }
        }

        if (events[eventIndex].observations[this.chartType]) {
          const value: any = parseFloat(events[eventIndex].observations[this.chartType]).toFixed(1);
          data.push([parseFloat(moment(events[eventIndex].date).format('x')), value * 1]);
        }
      }
      currentDate.add(1, 'd');
    }
    {
      setTimeout(() => {
        {
          Highcharts.stockChart({
            chart: {
              renderTo: this.chartId,
              type: 'line',
              height: 180,
              width: 250,
            },
            title: {
              text: ''
            },

            tooltip: {
              valueSuffix: ''
            },
            yAxis: {
              opposite: false,
              lineWidth: 2,
              showEmpty: true,

              title: {
                text: ''
              },
              plotOptions: {
                series: {
                  marker: {
                    fillColor: '#FFFFFF',
                    lineWidth: 2,
                    lineColor: null // inherit from series
                  }
                }
              }
            },
            navigator: {
              enabled: false
            },
            exporting: {
              enabled: false
            }, credits: {
              enabled: false
            },
            scrollbar: {
              enabled: false
            },
            rangeSelector: {
              enabled: false
            },
            xAxis: {
              type: 'datetime',
              min: new Date(start).getTime(),
              max: new Date(end).getTime(),
              ordinal: false,
            },
            series: [{
              name: '',
              pointWidth: 40,
              dateTimeLabelFormats: {
                hour: '%I %p',
                minute: '%I:%M'
              },
              marker: {
                enabled: true,
                fillColor: '#FFFFFF',
                lineWidth: 2,
                lineColor: null // inherit from series
              },
              data: data
            }]
          });
        }
      });
    }
    console.log(data);
  }
  isSameDayAndMonth(m1, m2) {
    return m1.date() === m2.date() && m1.month() === m2.month();
  }

}
