import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ActivationEnd } from '@angular/router';
import { CommonServiceService } from '../common-service.service';
import { Observable } from 'rxjs';
import { JSONP_ERR_WRONG_METHOD } from '@angular/common/http/src/jsonp';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  loadingText: string;
  errorMessage: any;
  apiUrl = 'https://api.genusconnect.com/v1/health';
  url_string1: any;
  url_string: any;
  url7: any;
  communityId: any;
  hospitalData: any;
  vitalSignData: any;
  loadIndicatorVisible = false;

  constructor(private router: Router, private commonService: CommonServiceService,private activatedRoute: ActivatedRoute)
    {
    //this.communityId = '6d3eb1a0-08dd-11e8-b39c-c11354535c16'
    this.url_string1 = window.location.href;
    this.url_string = this.url_string1.replace('/#', '');
    this.url7 = new URL(this.url_string);
    this.communityId = this.url7.searchParams.get("communityId");
    this.activatedRoute.queryParams.subscribe(params => this.communityId = params['communityId']);
    console.log(this.communityId);
    // this.hospitalData = [
    //   { firstName: 'Girish', lastName: 'Nediyadath', email: 'nedgir2412@gmail.com', communityId: this.communityId }
    // ];
    this.hospitalData = [
      { communityId: this.communityId }
    ];
    this.errorMessage = '';
    localStorage.setItem('communityId',this.communityId);
  }

  ngOnInit() {
    this.hospitalData.forEach(element => {
      this.getHeartRate(element);
    });
  }

  // getCommunityDetails(){

  // }

  logout() {
    this.router.navigate(['login']);
  }
  
  // print(): void {
  //   let printContents, popupWin;
  //   printContents = document.getElementById('print-section').innerHTML;
  //   popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
  //   popupWin.document.open();
  //   popupWin.document.write(`
  //     <html>
  //       <head>
  //         <title>Print tab</title>
  //         <style>
  //         //........Customized style.......
  //         </style>
  //       </head>
  //   <body onload="window.print();window.close()">${printContents}</body>
  //     </html>`
  //   );
  //   popupWin.document.close();
  // }
  getHeartRate(item) {
    this.heartDataEntry(item);
  }
  heartDataEntry(item) {
    this.loadingText = 'Please wait, data is loading...';
    const url = `https://api.genusconnect.com/v1/health/dataentry?communityId=${item.communityId}`;
    this.commonService.getHeartRate(url)
      .subscribe(results => {
        this.commonService.saveHeartData(results.items);
        this.observations(item);
      },
        error => {
          this.errorMessage = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
          }, 3000);
        }
      );
  }
  observations(item) {
    this.loadingText = 'Please wait, data is loading...';
    const url = `https://api.genusconnect.com/v1/community/observation?communityId=${item.communityId}`;
    this.commonService.getObservations(url)
      .subscribe(results => {
        this.commonService.saveObservationsData(results.items);
        this.pocEmergency(item);
      },
        error => {
          this.errorMessage = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
          }, 3000);
        }
      );
  }
  pocEmergency(item) {
    const url = `${this.apiUrl}/personalinformation?communityId=${item.communityId}`;
    this.commonService.pocEmergency(url)
      .subscribe(results => {
        this.commonService.savePocEmergency(results.items);
        this.getTargetRange(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        });
  }
  getTargetRange(item) {
    const url = `${this.apiUrl}/targetrange?communityId=${item.communityId}`;
    this.commonService.getTargetRange(url)
      .subscribe(results => {
        this.commonService.saveTargetRange(results.items);
        this.medicalVisitData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  medicalVisitData(item) {
    const url = `${this.apiUrl}/doctorvisit?communityId=${item.communityId}`;
    this.commonService.medicalVisitData(url)
      .subscribe(results => {
        this.commonService.saveMedicalVisitData(results.items);
        this.surgeryData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  surgeryData(item) {
    const url = `${this.apiUrl}/surgery?communityId=${item.communityId}`;
    this.commonService.surgeryData(url)
      .subscribe(results => {
        this.commonService.saveSurgeryData(results.items);
        this.alleryData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  alleryData(item) {
    const url = `${this.apiUrl}/allergy?communityId=${item.communityId}`;
    this.commonService.alleryData(url)
      .subscribe(results => {
        this.commonService.saveAlleryData(results.items);
        this.medicalDocumentData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  medicalDocumentData(item) {
    const url = `${this.apiUrl}/medicaldocument?communityId=${item.communityId}`;
    this.commonService.medicalDocumentData(url)
      .subscribe(results => {
        this.commonService.saveMedicalDocumentData(results.items);
        this.insuranceData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  insuranceData(item) {
    const url = `${this.apiUrl}/insurance?communityId=${item.communityId}`;
    this.commonService.insuranceData(url)
      .subscribe(results => {
        this.commonService.saveInsuranceData(results.items);
        this.familyHistoryData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  familyHistoryData(item) {
    const url = `${this.apiUrl}/familyhistory?communityId=${item.communityId}`;
    this.commonService.familyHistoryData(url)
      .subscribe(results => {
        this.commonService.saveFamilyHistoryData(results.items);
        this.medicalDeviceData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  medicalDeviceData(item) {
    const url = `${this.apiUrl}/medicaldevices?communityId=${item.communityId}`;
    this.commonService.medicalDeviceData(url)
      .subscribe(results => {
        this.commonService.saveMedicalDeviceData(results.items);
        this.physicianData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  physicianData(item) {
    const url = `${this.apiUrl}/doctor?communityId=${item.communityId}`;
    this.commonService.physicianData(url)
      .subscribe(results => {
        this.commonService.savePhysicianData(results.items);
        this.medication(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  medication(item) {
    const url = `${this.apiUrl}/medication?communityId=${item.communityId}`;
    this.commonService.medication(url)
      .subscribe(results => {
        this.commonService.saveMedication(results.items);
        this.medicalConditionData(item);
      },
        error => {
          this.loadingText = '';
          this.errorMessage = 'Server Error, Please try again !!';
          setTimeout(() => {
            this.errorMessage = '';
          }, 3000);
        }
      );
  }
  medicalConditionData(item) {
    const url = `${this.apiUrl}/medicalcondition?communityId=${item.communityId}`;
    this.commonService.medicalConditionData(url)
      .subscribe(results => {
        this.commonService.saveMedicalConditionData(results.items);
        this.loadingText = '';
        this.router.navigate(['careReport']);
      },
        error => {
          console.log(error);
        });
  }
}
