import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateCareReportComponent } from './generate-care-report.component';

describe('GenerateCareReportComponent', () => {
  let component: GenerateCareReportComponent;
  let fixture: ComponentFixture<GenerateCareReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateCareReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateCareReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
