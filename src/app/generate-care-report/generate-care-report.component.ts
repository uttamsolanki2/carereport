import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CommonServiceService } from '../common-service.service';
import { _ } from 'underscore';
import {UUID} from 'angular2-uuid';
import { HttpClient } from '@angular/common/http';
import * as jsPDF from 'jspdf'
import html2canvas from 'html2canvas';
//import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-generate-care-report',
  templateUrl: './generate-care-report.component.html',
  styleUrls: ['./generate-care-report.component.css']
})
export class GenerateCareReportComponent implements OnInit {

  @ViewChild('printSection')printSection:ElementRef;
  treatmentArray = [];
  data = {
    assignedBy: '',
    practice: ''
  };
  medicalDeviceList: any;
  physician = [];
  _currentInformation: any;
  emergencyContactInfo = [];
  PersonalData = [];
  _currentDate: string;
  sleepPattern = [];
  bodyTemperature = [];
  respiratoryRate = [];
  oxygenLevel = [];
  glucoselevel = [];
  bloodpresure = [];
  heartRateData = [];
  healthDataEntry = [];
  medicalDeviceInfo = [];
  familyHistory = [];
  targetRange: any;
  pocEmegency = [];
  medicalConditionInfo = [];
  surgeriesInfo = [];
  allergy = [];
  familyHistroydata = [];
  insurancelistData = [];
  medicaldocument = [];
  medicationInfo = [];
  physicianData = [];
  hrHistoryChart = {};
  btempHistoryChart = {};
  olHistoryChart = {};
  glHistoryChart = {};
  bpChart = [];
  heartRateChart = [];
  respiratoryChart = [];
  oxygenLevelChart = [];
  glucoselevelChart = [];
  BodyTemperatureChart = [];

  communityId: any;
  loadingText: string;
  errorMessage: any;
  apiUrl = 'https://api.genusconnect.com/v1/health';
  toEmail: any;

  constructor(private router: Router, private commonService: CommonServiceService, private http:HttpClient) {
    this.communityId = localStorage.getItem('communityId');
    this._currentDate = new Date().toLocaleDateString();
    this.targetRange = {
      hr: '-',
      bp: '-',
      rrate: '-',
      btemp: '-',
      ol: '-',
      gl: '-'
    };
    this.bpChart = [{
      type: 'bp',
      duration: 'weekly',
      label: 'Heart Beat',
      chartId: 'bp-chart-weekly',
      labelString: 'bpm'
    },
    {
      type: 'bp',
      duration: 'monthly',
      chartId: 'bp-chart-monthly',
    },
    {
      type: 'bp',
      duration: 'yearly',
      chartId: 'bp-chart-yearly'
    }];

    this.heartRateChart = [{
      type: 'hr',
      duration: 'weekly',
      label: 'Heart Beat',
      labelString: 'bpm',
      chartId: 'hr-chart-weekly'
    },
    {
      type: 'hr',
      duration: 'monthly',
      label: 'Heart Beat',
      labelString: 'bpm',
      chartId: 'hr-chart-monthly'
    },
    {
      type: 'hr',
      duration: 'yearly',
      label: 'Heart Beat',
      labelString: 'bpm',
      chartId: 'hr-chart-yearly'
    }];
    this.hrHistoryChart = {
      type: 'hr',
      duration: 'monthly',
      label: 'Heart Beat',
      labelString: 'bpm',
      chartId: 'hr-history-chart-monthly'
    };

    this.respiratoryChart = [{
      type: 'rrate',
      label: 'Respiratory Level',
      labelString: 'bpm',
      duration: 'weekly',
      chartId: 'rrate-chart-weekly'
    },
    {
      type: 'rrate',
      label: 'Respiratory Level',
      labelString: 'bpm',
      duration: 'monthly',
      chartId: 'rrate-chart-monthly'
    },
    {
      type: 'rrate',
      label: 'Respiratory Level',
      labelString: 'bpm',
      duration: 'yearly',
      chartId: 'rrate-chart-yearly'
    }];

    this.oxygenLevelChart = [{
      type: 'ol',
      label: 'Oxygen level',
      labelString: '%spO2',
      duration: 'weekly',
      chartId: 'ol-chart-weekly'
    },
    {
      type: 'ol',
      label: 'Oxygen level',
      labelString: '%spO2',
      duration: 'monthly',
      chartId: 'ol-chart-monthly'
    },
    {
      type: 'ol',
      label: 'Oxygen level',
      labelString: '%spO2',
      duration: 'yearly',
      chartId: 'ol-chart-yearly'
    }
    ];
    this.olHistoryChart = {
      type: 'ol',
      label: 'Oxygen level',
      labelString: '%spO2',
      duration: 'monthly',
      chartId: 'ol-history-chart-monthly'
    };

    this.glucoselevelChart = [{
      type: 'gl',
      label: 'Glucose Levels',
      labelString: 'mg/dL',
      duration: 'weekly',
      chartId: 'gl-chart-weekly'
    }, {
      type: 'gl',
      label: 'Glucose Levels',
      labelString: 'mg/dL',
      duration: 'monthly',
      chartId: 'gl-chart-monthly'
    }, {
      type: 'gl',
      label: 'Glucose Levels',
      labelString: 'mg/dL',
      duration: 'yearly',
      chartId: 'gl-chart-yearly'
    }];

    this.glHistoryChart = {
      type: 'gl',
      label: 'Glucose Levels',
      labelString: 'mg/dL',
      duration: 'monthly',
      chartId: 'gl-history-chart-monthly'
    };

    this.BodyTemperatureChart = [{
      type: 'btemp',
      label: 'Body Temparature',
      labelString: 'F',
      duration: 'weekly',
      chartId: 'btemp-chart-weekly'
    }, {
      type: 'btemp',
      label: 'Body Temparature',
      labelString: 'F',
      duration: 'monthly',
      chartId: 'btemp-chart-monthly'
    }, {
      type: 'btemp',
      label: 'Body Temparature',
      labelString: 'F',
      duration: 'yearly',
      chartId: 'btemp-chart-yearly'
    }];

    this.btempHistoryChart = {
      type: 'btemp',
      label: 'Boy Temparature',
      labelString: 'F',
      duration: 'monthly',
      chartId: 'btemp-history-chart-monthly'
    };

    this._currentInformation = {
      allergy: '',
      medical_conditions: '',
      current_medicines: '',
      medical_devices: '',
      medical_visit: ''
    };
  }
  ngOnInit() {
    console.log("in")
    setTimeout((n)=>{
      console.log("clicked")
      this.generatePdf();
    }, 2000);
    this.getTargetRange();
    this.getPocData();
    this.getHealthDataEntryGet();
    this.getMedicationData();
    this.getPhysicianData();
    this.getMedicalDeviceData();
    this.getfamilyHistoryData();
    this.getInsuranceData();
    this.getMedicalDocumentData();
    this.getAlleryData();
    this.getFamilyHistoryData();
    this.getSurgery();
    this.getMedicalCondition();
    this.getmedicalVisitData();
    console.log("inside");
  }
  getTargetRange() {
    const ranges = this.commonService.storeTargetRange;
    let rangeData = ranges.filter(x => {
      return x.name === 'hr';
    });
    if (rangeData.length > 0) {
      this.targetRange.hr = rangeData[0].min + ' - ' + rangeData[0].max + 'bpm';
    }
    rangeData = ranges.filter(x => {
      return x.name === 'bp';
    });
    if (rangeData.length > 0) {
      this.targetRange.bp = rangeData[0].min + ' - ' + rangeData[0].max + 'mmHg';
    }
    rangeData = ranges.filter(x => {
      return x.name === 'rrate';
    });
    if (rangeData.length > 0) {
      this.targetRange.rrate = rangeData[0].min + ' - ' + rangeData[0].max + 'bpm';
    }
    rangeData = ranges.filter(x => {
      return x.name === 'ol';
    });
    if (rangeData.length > 0) {
      this.targetRange.ol = rangeData[0].min + ' - ' + rangeData[0].max + '% SpO<sub>2</sub>.';
    }
    rangeData = ranges.filter(x => {
      return x.name === 'gl';
    });
    if (rangeData.length > 0) {
      this.targetRange.gl = rangeData[0].min + ' - ' + rangeData[0].max + 'mg/dL';
    }
    rangeData = ranges.filter(x => {
      return x.name === 'btemp';
    });
    if (rangeData.length > 0) {
      this.targetRange.btemp = rangeData[0].min + ' - ' + rangeData[0].max + '°F';
    }
  }
  getPocData() {
    this.pocEmegency = this.commonService.storePocEmergency;
    if (this.pocEmegency && this.pocEmegency.length > 0) {
      const pocData1 = this.pocEmegency.filter(x => x.isEmergencyContact === 0);
      if (pocData1 && pocData1.length > 0) {
        this.PersonalData.push(pocData1[0]);
        console.log(this.PersonalData);
      } else {
        this.PersonalData = [];
      }
      for (let i = 0; i < this.PersonalData.length; i++) {
        const item = this.PersonalData[i];
        item.bmi = item.height === 0 ? 0 : Math.round(item.weight * 0.4536 / item.height / item.height / 0.0254 / 0.0254);
      }
      const emergencyContactData = this.pocEmegency.filter(x => x.isEmergencyContact === 1);
      const emergencyContactDatalist = _.uniq(emergencyContactData, 'guid').reverse();

      if (emergencyContactDatalist[0]) {
        this.emergencyContactInfo.push(emergencyContactDatalist[0]);
        console.log(this.emergencyContactInfo);
      } else {
        this.emergencyContactInfo = [];
      }
    }
  }
  getHealthDataEntryGet() {
    this.healthDataEntry = this.commonService.storeHealthDataEntry;
    this.healthDataEntry.forEach(x => {
      if (x.type === 'hr') {
        this.heartRateData.push(x);
      }
      if (x.type === 'bp') {
        this.bloodpresure.push(x);
      }
      if (x.type === 'gl') {
        this.glucoselevel.push(x);
      }
      if (x.type === 'ol') {
        this.oxygenLevel.push(x);
      }
      if (x.type === 'rrate') {
        this.respiratoryRate.push(x);
      }
      if (x.type === 'btemp') {
        this.bodyTemperature.push(x);
      }
      if (x.type === 'sp') {
        this.sleepPattern.push(x);
      }
    });
  }
  getmedicalVisitData() {
    const medicalVisits = this.commonService.storeMedicalVisitData;
    if (medicalVisits.length) {
      const medicalVisitList = _.uniq(medicalVisits, 'guid').reverse();
      const medicalVisit = medicalVisitList[0];
      this._currentInformation.medical_visit = 'Last visit: ' + medicalVisit.time + ' ' + medicalVisit.reason;
    }
  }
  getMedicalCondition() {
    const medicalCondition = this.commonService.storeMedicalConditionData;
    if (medicalCondition && medicalCondition.length) {
      const medicalConditionDataList = _.uniq(medicalCondition, 'guid').reverse();
      if (medicalConditionDataList.length > 0) {
        this.medicalConditionInfo = medicalConditionDataList;
      } else {
        this.medicalConditionInfo = [];
      }
      for (let i = 0; i < this.medicalConditionInfo.length; i++) {
        const item = this.medicalConditionInfo[i];
        if (this._currentInformation.medical_conditions.length > 0) {
          this._currentInformation.medical_conditions += ', ';
        }
        this._currentInformation.medical_conditions += item['name'];
      }
    }
  }
  getSurgery() {
    const surgeries = this.commonService.storeSurgeryData;
    if (surgeries && surgeries.length > 0) {
      const surgeriesDataList = _.uniq(surgeries, 'guid').reverse();
      if (surgeriesDataList.length > 0) {
        this.surgeriesInfo = surgeriesDataList;
      } else {
        this.surgeriesInfo = [];
      }
    }
  }
  getFamilyHistoryData() {
    const familyHistory = this.commonService.storeFamilyHistoryData;
    if (familyHistory && familyHistory.length > 0) {
      const familyHistroyInfo = _.uniq(familyHistory, 'guid').reverse();
      if (familyHistroyInfo.length > 0) {
        this.familyHistroydata = familyHistroyInfo;
      } else {
        this.familyHistroydata = [];
      }
    }
  }
  getAlleryData() {
    const allergies = this.commonService.storeAlleryData;
    if (allergies && allergies.length > 0) {
      const allergiesData = _.uniq(allergies, 'guid').reverse();
      if (allergiesData.length > 0) {
        this.allergy = allergiesData;
      } else {
        this.allergy = [];
      }
      for (let i = 0; i < this.allergy.length; i++) {
        const item = this.allergy[i];
        if (this._currentInformation.allergy.length > 0) {
          this._currentInformation.allergy += ', ';
        }
        this._currentInformation.allergy += item['type'];
      }
    }
  }
  getMedicalDocumentData() {
    const medicalDocument = this.commonService.storeMedicalDocumentData;
    if (medicalDocument && medicalDocument.length > 0) {
      const medicalDocumentInfo = _.uniq(medicalDocument, 'guid').reverse();
      if (medicalDocumentInfo.length > 0) {
        this.medicaldocument = this.medicalConditionInfo;
      } else {
        this.medicaldocument = [];
      }
    }
  }
  getInsuranceData() {
    const insurance = this.commonService.storeInsuranceData;
    if (insurance && insurance.length > 0) {
      const insurancelist = _.uniq(insurance, 'guid').reverse();
      if (insurancelist.length > 0) {
        this.insurancelistData = insurancelist;
      } else {
        this.insurancelistData = [];
      }
    }
  }
  getfamilyHistoryData() {
    const familyHistory = this.commonService.storeFamilyHistoryData;
    if (familyHistory && familyHistory.length > 0) {
      const familyHistroyInfo = _.uniq(familyHistory, 'guid').reverse();
      if (familyHistroyInfo.length > 0) {
        this.familyHistroydata = familyHistroyInfo;
      } else {
        this.familyHistroydata = [];
      }
    }
  }
  getMedicalDeviceData() {
    const medicalDevice = this.commonService.storeMedicalDeviceData;
    if (medicalDevice && medicalDevice.length > 0) {
      this.medicalDeviceList = _.uniq(medicalDevice, 'guid').reverse();
      this.medicalDeviceInfo = this.medicalDeviceList;
      for (let i = 0; i < this.medicalDeviceInfo.length; i++) {
        const item = this.medicalDeviceInfo[i];
        if (this._currentInformation.medical_devices.length > 0) {
          this._currentInformation.medical_devices += ', ';
        }
        this._currentInformation.medical_devices += item['deviceName'];
      }
    }
    const physiciandata = this.commonService.storePhysicianData;
    if (physiciandata && physiciandata.length > 0) {
      this.physician = _.uniq(physiciandata, 'guid').reverse();
    }
    if (this.physician && this.physician.length !== 0 && this.medicalDeviceList && this.medicalDeviceList.length !== 0) {
      const treatmentArray = this.physician.filter(x => x.name.trim() === this.medicalDeviceList[0].assignedBy.trim());
      if (this.medicalDeviceList.length !== 0 && treatmentArray.length !== 0) {
        this.data.assignedBy = this.medicalDeviceList[0].assignedBy;
        this.data.practice = this.treatmentArray[0].practice;
      }
    } else {
      this.treatmentArray = [];
    }
  }
  getPhysicianData() {
    const physiciandata = this.commonService.storePhysicianData;
    if (physiciandata && physiciandata.length > 0) {
      const physician = _.uniq(physiciandata, 'guid').reverse();
      if (physician.length > 0) {
        this.physicianData = this.physician;
      } else {
        this.physicianData = [];
      }
    }
  }
  getMedicationData() {
    const medication = this.commonService.storeMedication;
    if (medication && medication.length > 0) {
      const medicationlist = _.uniq(medication, 'guid').reverse();
      if (medicationlist.length > 0) {
        this.medicationInfo = medicationlist;
      } else {
        this.medicationInfo = [];
      }
      for (let i = 0; i < this.medicationInfo.length; i++) {
        const item = this.medicationInfo[i];
        if (this._currentInformation.current_medicines.length > 0) {
          this._currentInformation.current_medicines += ', ';
        }
        this._currentInformation.current_medicines += item['name'];
      }
    }
    // setTimeout((n)=>{
    //   console.log("clicked")
    //   this.generatePdf();
    // }, 1000)
  }
  back() {
    this.router.navigate(['login']);
  }
  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
        <html>
          <head>
            <title>Print tab</title>
             <style>
             @import url('../../assets/css/bootstrap.css');
             @import "../../assets/css/helpers.css";
             @import "../../assets/css/pdf.css";
 </style>
  </head>
 <body onload="window.print();window.close()">${printContents}</body>
 </html>`
    );
    popupWin.document.close();
  }

  downloadPdf(){
    // var doc = new jsPDF();
    // doc.text(20, 20, 'Hello world!');
    // doc.text(20, 30, 'This is client-side Javascript, pumping out a PDF.');
    // doc.addPage();
    // doc.text(20, 20, 'Do you like that?');
    // // Save the PDF
    let doc = new jsPDF();
    doc.addHTML(document.getElementById("printSection"), function() {
       doc.save("obrz.pdf");
    });
   
  }


  generatePdf() {
    const div = document.getElementById("printSection");
    const options = {};

    
    html2canvas(div,{allowTaint:true}).then(function(canvas) {
      
      

    var HTML_Width = canvas.width;
    var HTML_Height = canvas.height;
    var top_left_margin = 20;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;
    
    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
      
    var imgData = canvas.toDataURL("image/jpeg", 1.0);
    
    var pdf = new jsPDF({orientation: 'p',unit: 'pt',format: [PDF_Width, PDF_Height]})

    var incanvas = document.createElement('canvas'),
        ctx = incanvas.getContext('2d'),
        parts = [],
        hy=0,
        img = new Image();
    img.onload = split_4;
    function split_4() {
      var w2 = img.width,
          h2 = PDF_Height
          hy = 0;
      for(var i=0; i<=totalPDFPages; i++) { 
        incanvas.width = w2;
        // incanvas.height = PDF_Height;
        incanvas.height = PDF_Height  - 100;
        ctx.fillStyle = "white";
        ctx.fillRect(0,0,canvas.width, canvas.height);
        ctx.drawImage(img,0,hy,img.width,PDF_Height, 0, 0, incanvas.width,incanvas.height);
         hy = hy+PDF_Height
        parts.push( incanvas.toDataURL("image/jpeg", 1.0) );
        pdf.addImage(parts[i], 'JPG', top_left_margin, top_left_margin,incanvas.width,incanvas.height-top_left_margin);
        if(i!=totalPDFPages)
          pdf.addPage([PDF_Width, PDF_Height]);
        var slicedImage = document.createElement('img')
        slicedImage.src = parts[i];
      //  document.body.appendChild(slicedImage)
      if( i == totalPDFPages){
        // console.log("op", imgData)
      }
      }
      console.log( parts );

    }
    img.src = imgData;
 
   setTimeout((n) => {
        
          // console.log('sd', pdf.link)
          // console.log("next", pdf.output)
          // pdf.output("careReport.pdf")
          // pdf.addHTML(div, 10, 10, options, function(){
            var blob = pdf.output("blob");
            // pdf.output(blob)
            window.open(URL.createObjectURL(blob), "_parent");
          // })
          // pdf.save("careReport.pdf");
          
          
        },1000);









      
      
        //pdf.save("careReport.pdf");
        
        });
}


  share() {
    debugger;
    console.log(this.toEmail);
    let data = {
      communityId: this.communityId,
      guid: UUID.UUID(),
      lastUpdateTimestamp: new Date(),
      toEmail: this.toEmail,
      link: "https://webapp.genusconnect.org/carereport/?communityId="+this.communityId
    }
    console.log(data);
    let url = "https://api.genusconnect.com/v1/carereport/share";
    this.http.post(url,data).subscribe(res=>{
      console.log(res);
      alert('care report shared successfully !');
    })
  }

  printPdf(){
    var data = document.getElementById('printSection');  
    let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
      var position = 0;
      pdf.addHTML(data,()=>{
        pdf.save('MYPdf.pdf'); // Generated PDF   
      })  
    
    html2canvas(data).then(canvas => {  
      // Few necessary setting options  
      var imgWidth = 208;   
      var pageHeight = 295;    
      var imgHeight = canvas.height * imgWidth / canvas.width;  
      var heightLeft = imgHeight;  
  
      const contentDataURL = canvas.toDataURL('image/png')  
      
     
    });  
  }

}
