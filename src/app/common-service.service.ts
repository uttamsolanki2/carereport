import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CommonServiceService {

  storeObservationData: any;
  storeMedication: any;
  storeMedicalConditionData: any;
  storeTargetRange: any;
  storePocEmergency: any;
  storeMedicalVisitData: any;
  storeSurgeryData: any;
  storeAlleryData: any;
  storeMedicalDocumentData: any;
  storeInsuranceData: any;
  storeFamilyHistoryData: any;
  storeMedicalDeviceData: any;
  storePhysicianData: any;
  storeHealthDataEntry: any;
  constructor(private http: HttpClient) { }
  getHeartRate(url): Observable<any> {
    return this.http.get(url);
  }
  getObservations(url): Observable<any> {
    return this.http.get(url);
  }
  pocEmergency(url): Observable<any> {
    return this.http.get(url);
  }
  getTargetRange(url): Observable<any> {
    return this.http.get(url);
  }
  medicalVisitData(url): Observable<any> {
    return this.http.get(url);
  }
  surgeryData(url): Observable<any> {
    return this.http.get(url);
  }
  alleryData(url): Observable<any> {
    return this.http.get(url);
  }
  medicalDocumentData(url): Observable<any> {
    return this.http.get(url);
  }
  insuranceData(url): Observable<any> {
    return this.http.get(url);
  }
  familyHistoryData(url): Observable<any> {
    return this.http.get(url);
  }
  medicalDeviceData(url): Observable<any> {
    return this.http.get(url);
  }
  physicianData(url): Observable<any> {
    return this.http.get(url);
  }
  medication(url): Observable<any> {
    return this.http.get(url);
  }
  medicalConditionData(url): Observable<any> {
    return this.http.get(url);
  }
  saveHeartData(data) {
    this.storeHealthDataEntry = data;
  }
  saveObservationsData(data) {
    this.storeObservationData = data;
  }
  savePhysicianData(data) {
    this.storePhysicianData = data;
  }
  saveMedicalDeviceData(data) {
    this.storeMedicalDeviceData = data;
  }
  saveFamilyHistoryData(data) {
    this.storeFamilyHistoryData = data;
  }
  saveInsuranceData(data) {
    this.storeInsuranceData = data;
  }
  saveMedicalDocumentData(data) {
    this.storeMedicalDocumentData = data;
  }
  saveAlleryData(data) {
    this.storeAlleryData = data;
  }
  saveSurgeryData(data) {
    this.storeSurgeryData = data;
  }
  saveMedicalVisitData(data) {
    this.storeMedicalVisitData = data;
  }
  savePocEmergency(data) {
    this.storePocEmergency = data;
  }
  saveTargetRange(data) {
    this.storeTargetRange = data;
  }
  saveMedicalConditionData(data) {
    this.storeMedicalConditionData = data;
  }
  saveMedication(data) {
    this.storeMedication = data;
  }
}
