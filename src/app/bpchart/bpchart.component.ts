import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { CommonServiceService } from '../common-service.service';
declare var Highcharts: any;

@Component({
  selector: 'app-bpchart',
  templateUrl: './bpchart.component.html',
  styleUrls: ['./bpchart.component.css']
})
export class BpchartComponent implements OnInit {
  options: any;
  leftLegendText: any;
  rightLegendText: any;
  chartData: any;
  @Input() chartType: any;
  @Input() chartId: any;

  constructor(private commonService: CommonServiceService) {
    this.chartData = [{ values: [] }];
  }

  ngOnInit() {
    this.switchChartType();
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  async getChartData(start, end) {
    await this.sleep(500);
    let events = [];
    let days = 1;
    const a = moment(end);
    const b = moment(start);
    switch (this.chartType) {
      case 'daily':
        days = 1;
        break;
      case 'weekly':
        days = 1;
        // days=7;
        break;
      case 'monthly':
        days = 30;
        days = a.diff(b, 'days');
        break;
      case 'yearly':
        days = 12;
        days = a.diff(b, 'days');
        break;
      default:
        break;
    }
    events = this.commonService.storeHealthDataEntry;
    const self = this;
    const filteredData = events.filter(x => {
      return x.type === 'bp' && x.communityId;
    });
    this.chartData[0] = filteredData.map(r => {
      const result = { date: parseFloat(moment(r.time).format('x')), open: r.value1, high: r.value2 };
      return [parseFloat(moment(r.time).format('x')), parseFloat(r.value1), parseFloat(r.value2)];
    });

    // tslint:disable-next-line:no-shadowed-variable
    this.chartData[0].sort(function (a, b) {
      return new Date(a[0]).getTime() - new Date(b[0]).getTime();
    });
    const ranges = this.commonService.storeTargetRange;
    const targetRange = { min: 0, max: 0 };
    const rangeData = ranges.filter(x => {
      return x.name === 'bl';
    });
    if (rangeData.length > 0) {
      targetRange.min = rangeData[0].min;
      targetRange.max = rangeData[0].max;
    }
    {

      Highcharts.stockChart(this.chartId, {
        chart: {
          type: 'columnrange',
          height: 180,
          width: 250,
        },
        plotOptions: {
          series: {
            pointWidth: 2
          }
        },
        tooltip: {
          valueSuffix: 'mmHg'
        },
        yAxis: {
          opposite: false,
          title: {
            text: 'mmHg'
          },
          plotBands: [{
            from: targetRange.min,
            to: targetRange.max,
            color: {
              linearGradient: { x1: 1, y1: 1, x2: 1, y2: 0 },
              stops: [
                [0, 'rgb(209, 209, 209)'],
                [1, 'rgb(254, 254, 254)']
              ]
            }

          }]
        },
        navigator: {
          enabled: false
        },
        exporting: {
          enabled: false
        }, credits: {
          enabled: false
        },
        scrollbar: {
          enabled: false
        },
        rangeSelector: {
          enabled: false
        },

        xAxis: {
          type: 'datetime',
          min: new Date(start).getTime(),
          max: new Date(end).getTime()

        },

        series: [{
          name: 'mmHg',
          dateTimeLabelFormats: {
            hour: '%I %p',
            minute: '%I:%M'
          },
          data: this.chartData[0]
        }]
      });
    }
  }
  async switchChartType() {
    let start;
    let end;
    switch (this.chartType) {
      case 'daily':
        start = moment()
          .startOf('day')
          .subtract(1, 'd');
        end = moment().endOf('day');
        this.leftLegendText = start.format('M/D/YY');
        this.rightLegendText = end.format('M/D/YY');
        break;
      case 'weekly':
        start = moment()
          .startOf('day')
          .subtract(1, 'w');
        end = moment().endOf('day');
        this.leftLegendText = start.format('M/D/YY');
        this.rightLegendText = end.format('M/D/YY');
        break;
      case 'monthly':
        start = moment()
          .startOf('date')
          .subtract(1, 'M');
        end = moment().endOf('date');
        this.leftLegendText = start.format('MMMM');
        this.rightLegendText = end.format('MMMM');
        break;
      case 'yearly':
        start = moment()
          .startOf('month')
          .subtract(1, 'y');
        end = moment().endOf('month');
        this.leftLegendText = start.format('YYYY');
        this.rightLegendText = end.format('YYYY');
        break;
      default:
        break;
    }
    this.getChartData(start, end);
  }


}

