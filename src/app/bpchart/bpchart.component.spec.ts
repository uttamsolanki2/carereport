import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BpchartComponent } from './bpchart.component';

describe('BpchartComponent', () => {
  let component: BpchartComponent;
  let fixture: ComponentFixture<BpchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BpchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BpchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
